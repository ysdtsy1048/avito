import sys
sys.path.append('/home/ubuntu/apl/avito-demand-prediction/')
import dataset as ds
import os
import copy
import pandas as pd
from gensim.models import Word2Vec
from random import shuffle
from keras.preprocessing.text import text_to_word_sequence
import numpy as np
import logging
import time
from tqdm import tqdm


logging.basicConfig(level=logging.INFO)
#use_cols = ['param_1','param_2','param_3','title', 'description']
#use_cols = ['title', 'description']
use_cols = ['description']

def load_text(start):
    print('Loading data...', end='')
    tic = time.time()
    #train = pd.read_csv(ds.train_active, usecols=use_cols, nrows= 2000000, skiprows=range(1, start))
    train_act = pd.read_csv(ds.train_active, usecols=use_cols)
    train     = pd.read_csv(ds.train, usecols=use_cols)
    test_act  = pd.read_csv(ds.test_active, usecols=use_cols)
    toc = time.time()
    print('Done in {:.1f}s'.format(toc-tic))
    #train['text'] = train['param_1'].str.cat([train2.param_2,train2.param_3,train2.title,train2.description], sep=' ',na_rep='')
    #train.drop(use_cols, axis = 1, inplace=True)
    #train = train2['text'].values
    train = train['description'].astype(str).values
    train = [text_to_word_sequence(text) for text in tqdm(train)]
    return train

model = Word2Vec(size=500, window=5,max_vocab_size=500000)

for k in range(15):
    update = False
    if k != 0:
        update = True
    train = load_text(k*1000000+1)
    model.build_vocab(train, update=update)
    model.train(train, total_examples=model.corpus_count, epochs=3)

model.save('description500.w2v')
