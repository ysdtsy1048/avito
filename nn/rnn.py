import sys
sys.path.append('/home/ubuntu/apl/avito-demand-prediction/')
import dataset as ds
from gensim.models import word2vec

import pandas as pd 
import numpy as np 
import time 
import gc 

np.random.seed(42)

from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import StratifiedKFold

from keras.models import Model
from keras.layers import Input, Dropout, Dense, Embedding, SpatialDropout1D, concatenate, Dot, Reshape, Add, Subtract, Concatenate
from keras.layers import GRU, Bidirectional, GlobalAveragePooling1D, GlobalMaxPooling1D, BatchNormalization, Conv1D, MaxPooling1D, Flatten
from keras.preprocessing.sequence import pad_sequences
from keras.preprocessing import text, sequence
from keras.callbacks import Callback
from keras import backend as K
from keras.models import Model
from keras.regularizers import l2

from keras import optimizers

from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import LabelEncoder, MinMaxScaler, StandardScaler

import warnings
warnings.filterwarnings('ignore')

import os
os.environ['OMP_NUM_THREADS'] = '4'

import threading
import multiprocessing
from multiprocessing import Pool, cpu_count
from contextlib import closing
cores = 4

from keras import backend as K
from keras.optimizers import RMSprop, Adam
from keras.callbacks import ModelCheckpoint, EarlyStopping

### rmse loss for keras
def root_mean_squared_error(y_true, y_pred):
    return K.sqrt(K.mean(K.square(y_true - y_pred))) 


def preprocess_dataset(dataset):
    
    t1 = time.time()
    ########## numeric data ##########
    dataset['price'] = dataset['price'].fillna(0).astype('float32')

    ######### categorical data ##########
    #dataset['param_1'].fillna(value='missing', inplace=True)
    #dataset['param_2'].fillna(value='missing', inplace=True)
    #dataset['param_3'].fillna(value='missing', inplace=True)
    dataset['param_1'] = dataset['param_1'].astype(str)
    dataset['param_2'] = dataset['param_2'].astype(str)
    dataset['param_3'] = dataset['param_3'].astype(str)
    dataset['param123'] = (dataset['param_1']+' '+dataset['param_2']+' '+dataset['param_3']).astype('category')
    del dataset['param_2'], dataset['param_3']
    gc.collect()
    dataset['category_name'] = dataset['category_name'].astype('category')
    dataset['parent_category_name'] = dataset['parent_category_name'].astype('category')
    dataset['region'] = dataset['region'].astype('category')
    dataset['city'] = dataset['city'].astype('category')
    dataset['weekday'] = pd.to_datetime(dataset['activation_date']).dt.weekday.astype('uint8')
    #dataset['week'] = pd.to_datetime(dataset['activation_date']).dt.week.astype('uint8')
    #dataset['day'] = pd.to_datetime(dataset['activation_date']).dt.day.astype('uint8')
    #dataset['wday'] = pd.to_datetime(dataset['activation_date']).dt.dayofweek.astype('uint8')
    del dataset['activation_date']
    gc.collect()
    dataset['image_top_1'] = dataset['image_top_1'].fillna('')
    dataset['image_code'] = dataset['image_top_1'].astype(str)
    del dataset['image_top_1']
    gc.collect()

    print("PreProcessing Function completed.")    
    return dataset

def keras_fit(train):
    
    t1 = time.time()
    #train['title_description']= (train['title']+" "+train['description']).astype(str)
    #del train['description'], train['title']
    #gc.collect()
    train['title']       = train['title'].astype(str)
    train['description'] = train['description'].astype(str)

    print("Start Tokenization.....")
    #tokenizer = text.Tokenizer(num_words = max_words_title_description)
    tokenizer_title       = text.Tokenizer(num_words = max_words_title)
    tokenizer_description = text.Tokenizer(num_words = max_words_description)
    #all_text = np.hstack([train['title_description'].str.lower()])
    title = np.hstack([train['title'].str.lower()])
    description = np.hstack([train['description'].str.lower()])
    tokenizer_title.fit_on_texts(title)
    tokenizer_description.fit_on_texts(description)
    del title, description
    gc.collect()
    
    ########## Label encoding ##########
    print("Loading Test for Label Encoding on Train + Test")
    use_cols_test = ['region', 'city', 'parent_category_name', 'category_name', 'param_1', 'param_2', 'param_3', 'image_top_1', 'activation_date']
    test = pd.read_csv(ds.test, usecols = use_cols_test)
    
    test['param_1']  = test['param_1'].astype(str)
    test['param_2']  = test['param_2'].astype(str)
    test['param_3']  = test['param_3'].astype(str)                       
    test['param123'] = (test['param_1']+' '+test['param_2']+' '+test['param_3']).astype(str)
    del test['param_2'], test['param_3']
    gc.collect()
    test['image_top_1'] = test['image_top_1'].fillna('')
    test['image_code']  = test['image_top_1'].astype(str)
    del test['image_top_1']
    gc.collect()
    test['weekday'] = pd.to_datetime(test['activation_date']).dt.weekday.astype('uint8')
    #test['week']   = pd.to_datetime(test['activation_date']).dt.week.astype('uint8')
    #test['day']    = pd.to_datetime(test['activation_date']).dt.day.astype('uint8')
    #test['wday']   = pd.to_datetime(test['activation_date']).dt.dayofweek.astype('uint8')
    del test['activation_date']
    gc.collect()
    
    ntrain = train.shape[0]
    DF = pd.concat([train, test], axis = 0)
    del train, test
    gc.collect()
    print(DF.shape)
    
    print("Start Label Encoding process....")
    le_region = LabelEncoder()
    le_region.fit(DF.region)
    
    le_city = LabelEncoder()
    le_city.fit(DF.city)
    
    le_category_name = LabelEncoder()
    le_category_name.fit(DF.category_name)
    
    le_parent_category_name = LabelEncoder()
    le_parent_category_name.fit(DF.parent_category_name)
    
    le_param_1 = LabelEncoder()
    le_param_1.fit(DF.param_1)
    
    le_param123 = LabelEncoder()
    le_param123.fit(DF.param123)
    
    le_image_code = LabelEncoder()
    le_image_code.fit(DF.image_code)
    
    le_weekday = LabelEncoder()
    le_weekday.fit(DF.weekday)
    #le_week = LabelEncoder()
    #le_week.fit(DF.week)
    #le_day = LabelEncoder()
    #le_day.fit(DF.day)
    #le_wday = LabelEncoder()
    #le_wday.fit(DF.wday)
    
    train = DF[0:ntrain]
    del DF 
    gc.collect()
    
    train['price'] = np.log1p(train['price'])
    train['avg_days_up_user'] = np.log1p(train['avg_days_up_user'])
    train['avg_times_up_user'] = np.log1p(train['avg_times_up_user'])
    train['n_user_items'] = np.log1p(train['n_user_items'])
    train['item_seq_number'] = np.log(train['item_seq_number'])
    print("Fit on Train Function completed.")
    
    return train, tokenizer_title, tokenizer_description, le_region, le_city, le_category_name, le_parent_category_name, le_param_1, le_param123, le_image_code, le_weekday


def keras_train_transform(dataset):
    
    t1 = time.time()
    
    #dataset['seq_title_description']= tokenizer.texts_to_sequences(dataset.title_description.str.lower())
    dataset['seq_title']       = tokenizer_title.texts_to_sequences(dataset.title.str.lower())
    dataset['seq_description'] = tokenizer_description.texts_to_sequences(dataset.description.str.lower())
    print("Transform done for test")
    print("Time taken for Sequence Tokens is"+str(time.time()-t1))
    del train['title'], train['description']
    gc.collect()

    dataset['region'] = le_region.transform(dataset['region'])
    dataset['city'] = le_city.transform(dataset['city'])
    dataset['category_name'] = le_category_name.transform(dataset['category_name'])
    dataset['parent_category_name'] = le_parent_category_name.transform(dataset['parent_category_name'])
    dataset['param_1'] = le_param_1.transform(dataset['param_1'])
    dataset['param123'] = le_param123.transform(dataset['param123'])
    dataset['weekday'] = le_weekday.transform(dataset['weekday'])
    #dataset['day'] = le_day.transform(dataset['day'])
    #dataset['week'] = le_week.transform(dataset['week'])
    #dataset['wday'] = le_wday.transform(dataset['wday'])
    dataset['image_code'] = le_image_code.transform(dataset['image_code'])
    
    print("Transform on test function completed.")
    
    return dataset
    
def keras_test_transform(dataset):
    
    t1 = time.time()
    #dataset['title_description']= (dataset['title']+" "+dataset['description']).astype(str)
    #del dataset['description'], dataset['title']
    #gc.collect()
    
    #dataset['seq_title_description'] = tokenizer.texts_to_sequences(dataset.title_description.str.lower())
    dataset['seq_title'] = tokenizer_title.texts_to_sequences(dataset.title.str.lower()).astype(str)
    dataset['seq_description'] = tokenizer_description.texts_to_sequences(dataset.description.str.lower()).astype(str)
    print("Transform done for test")
    print("Time taken for Sequence Tokens is"+str(time.time()-t1))
    
    del dataset['title'], dataset['description']
    gc.collect()

    dataset['region'] = le_region.transform(dataset['region'])
    dataset['city'] = le_city.transform(dataset['city'])
    dataset['category_name'] = le_category_name.transform(dataset['category_name'])
    dataset['parent_category_name'] = le_parent_category_name.transform(dataset['parent_category_name'])
    dataset['param_1'] = le_param_1.transform(dataset['param_1'])
    dataset['param123'] = le_param123.transform(dataset['param123'])
    dataset['weekday'] = le_day.transform(dataset['weekday'])
    #dataset['day'] = le_day.transform(dataset['day'])
    #dataset['week'] = le_week.transform(dataset['week'])
    #dataset['wday'] = le_wday.transform(dataset['wday'])
    dataset['image_code'] = le_image_code.transform(dataset['image_code'])
    dataset['price'] = np.log1p(dataset['price'])
    dataset['item_seq_number'] = np.log(dataset['item_seq_number'])
    dataset['avg_days_up_user'] = np.log1p(dataset['avg_days_up_user'])
    dataset['avg_times_up_user'] = np.log1p(dataset['avg_times_up_user'])
    dataset['n_user_items'] = np.log1p(dataset['n_user_items'])
    
    print("Transform on test function completed.")
    
    return dataset
    
def get_keras_data(dataset):
    X = {
        #'seq_title_description': pad_sequences(dataset.seq_title_description, maxlen=max_seq_title_description_length)
        'seq_title': pad_sequences(dataset.seq_title, maxlen=max_seq_title_length)
        ,'seq_description': pad_sequences(dataset.seq_description, maxlen=max_seq_description_length)
        ,'region': np.array(dataset.region)
        ,'city': np.array(dataset.city)
        ,'category_name': np.array(dataset.category_name)
        ,'parent_category_name': np.array(dataset.parent_category_name)
        ,'param_1': np.array(dataset.param_1)
        ,'param123': np.array(dataset.param123)
        ,'weekday': np.array(dataset.weekday)
        ,'image_code':np.array(dataset.image_code)
        ,'avg_ad_days':np.array(dataset.avg_days_up_user )
        ,'avg_ad_times':np.array(dataset.avg_times_up_user)
        ,'n_user_items':np.array(dataset.n_user_items)
        ,'price': np.array(dataset[["price"]])
        ,'item_seq_number': np.array(dataset[["item_seq_number"]])
    }
    
    print("Data ready for Vectorization")
    
    return X

# Loading Train data - No Params, No Image data 
dtypes_train = {
                'price': 'float32',
                'deal probability': 'float32',
                'item_seq_number': 'uint32'
}

# No user_id
use_cols = ['item_id', 'user_id', 'image_top_1', 'region', 'city', 'parent_category_name', 'category_name', 'param_1', 'param_2', 'param_3', 'title', 'description', 'price', 'item_seq_number', 'activation_date', 'deal_probability']
train = pd.read_csv(ds.train, parse_dates=["activation_date"], usecols = use_cols, dtype = dtypes_train)

train_features = pd.read_csv(ds.aggregated_features)
train = train.merge(train_features, on = ['user_id'], how = 'left')
del train_features
gc.collect()

train['avg_days_up_user'] = train['avg_days_up_user'].fillna(0).astype('uint32')
train['avg_times_up_user'] = train['avg_times_up_user'].fillna(0).astype('uint32')
train['n_user_items'] = train['n_user_items'].fillna(0).astype('uint32')

y_train = np.array(train['deal_probability'])

del train['deal_probability']
gc.collect()

max_seq_title_length       = 300
max_seq_description_length = 500
max_words_title            = 100000
max_words_description      = 100000
train = preprocess_dataset(train)
train, tokenizer_title, tokenizer_description, le_region, le_city, le_category_name, le_parent_category_name, le_param_1, le_param123, le_image_code, le_weekday = keras_fit(train)
train = keras_train_transform(train)
print("Tokenization done and TRAIN READY FOR Validation splitting")

# Calculation of max values for Categorical fields 

max_region               = np.max(train.region.max())+2
max_city                 = np.max(train.city.max())+2
max_category_name        = np.max(train.category_name.max())+2
max_parent_category_name = np.max(train.parent_category_name.max())+2
max_param_1              = np.max(train.param_1.max())+2
max_param123             = np.max(train.param123.max())+2
max_weekday              = np.max(train.weekday.max())+2
#max_week                 = np.max(train.week.max())+2
#max_day                  = np.max(train.day.max())+2
#max_wday                 = np.max(train.wday.max())+2
max_image_code           = np.max(train.image_code.max())+2


del train['item_id'], train['user_id']
gc.collect()

# EMBEDDINGS COMBINATION 
EMBEDDING_DIM1 = 300
EMBEDDING_DIM2 = 500
# word2vec
embeddings_index1 = word2vec.Word2Vec.load(ds.title300)
embeddings_index2 = word2vec.Word2Vec.load(ds.description500)
# fasttext
#def get_coefs(word, *arr): return word, np.asarray(arr, dtype='float32')
#embeddings_index = dict(get_coefs(*o.rstrip().rsplit(' ')) for o in open(ds.fasttext2))

word_index1       = tokenizer_title.word_index
word_index2       = tokenizer_description.word_index
vocab_size1       = min(max_words_title, len(word_index1))
vocab_size2       = min(max_words_description, len(word_index2))
embedding_matrix1 = np.zeros((vocab_size1, EMBEDDING_DIM1))
embedding_matrix2 = np.zeros((vocab_size2, EMBEDDING_DIM2))

for word, i in word_index1.items():
    if i >= max_words_title: continue
    try:
        embedding_vector1 = embeddings_index1[word]
    except KeyError:
        embedding_vector1 = None
    if embedding_vector1 is not None: embedding_matrix1[i] = embedding_vector1

for word, i in word_index2.items():
    if i >= max_words_description: continue
    try:
        embedding_vector2 = embeddings_index2[word]
    except KeyError:
        embedding_vector2 = None
    if embedding_vector2 is not None: embedding_matrix2[i] = embedding_vector2

del embeddings_index1, embeddings_index2
gc.collect()
print("WORD2VEC DONE")

print(vocab_size1)
print(vocab_size2)



def RNN_model():
    ########## parametar ##########
    embedding_reg = 0.0002

    ########## categorical layers ##########
    # input
    region                = Input(shape=[1], name="region")               #categorical
    city                  = Input(shape=[1], name="city")                 #categorical
    category_name         = Input(shape=[1], name="category_name")        #categorical
    parent_category_name  = Input(shape=[1], name="parent_category_name") #categorical
    param_1               = Input(shape=[1], name="param_1")              #categorical
    param123              = Input(shape=[1], name="param123")             #categorical
    image_code            = Input(shape=[1], name="image_code")           #categorical
    
    # bias
    bias_emb_region                = Embedding(vocab_size1, 1, embeddings_regularizer=l2(embedding_reg))(region)
    bias_emb_city                  = Embedding(vocab_size1, 1, embeddings_regularizer=l2(embedding_reg))(city)
    bias_emb_category_name         = Embedding(vocab_size1, 1, embeddings_regularizer=l2(embedding_reg))(category_name)
    bias_emb_parent_category_name  = Embedding(vocab_size1, 1, embeddings_regularizer=l2(embedding_reg))(parent_category_name)
    bias_emb_param_1               = Embedding(vocab_size1, 1, embeddings_regularizer=l2(embedding_reg))(param_1)
    bias_emb_param123              = Embedding(vocab_size1, 1, embeddings_regularizer=l2(embedding_reg))(param123)
    bias_emb_image_code            = Embedding(vocab_size1, 1, embeddings_regularizer=l2(embedding_reg))(image_code)  
    biases = [
        Flatten() (bias_emb_region)
      , Flatten() (bias_emb_city)
      , Flatten() (bias_emb_category_name)
      , Flatten() (bias_emb_parent_category_name)
      , Flatten() (bias_emb_param_1)
      , Flatten() (bias_emb_param123)
      , Flatten() (bias_emb_image_code)   
     ]

    # factors
    emb_region                = Embedding(vocab_size1, 50, embeddings_regularizer=l2(embedding_reg))(region)
    emb_city                  = Embedding(vocab_size1, 50, embeddings_regularizer=l2(embedding_reg))(city)
    emb_category_name         = Embedding(vocab_size1, 50, embeddings_regularizer=l2(embedding_reg))(category_name)
    emb_parent_category_name  = Embedding(vocab_size1, 50, embeddings_regularizer=l2(embedding_reg))(parent_category_name)
    emb_param_1               = Embedding(vocab_size1, 50, embeddings_regularizer=l2(embedding_reg))(param_1)
    emb_param123              = Embedding(vocab_size1, 50, embeddings_regularizer=l2(embedding_reg))(param123)
    emb_image_code            = Embedding(vocab_size1, 50, embeddings_regularizer=l2(embedding_reg))(image_code)  
    factors = [
        Flatten() (emb_region)
      , Flatten() (emb_city)
      , Flatten() (emb_category_name)
      , Flatten() (emb_parent_category_name)
      , Flatten() (emb_param_1)
      , Flatten() (emb_param123)
      , Flatten() (emb_image_code)   
     ]
    s     = Add()(factors)
    diffs = [Subtract()([s, x]) for x in factors]
    dots  = [Dot(axes=1)([d, x]) for d,x in zip(diffs, factors)]
    x     = Concatenate()(biases + dots)
    categorical_layer = BatchNormalization()(x)

    ########## text layers ##########
    seq_title       = Input(shape=[300], name="seq_title")          #word2vec
    emb_seq_title   = Embedding(vocab_size1, EMBEDDING_DIM1, weights = [embedding_matrix1], trainable = True)(seq_title)
    title_layer     = SpatialDropout1D(0.2)(emb_seq_title)  
#    title_layer     = Bidirectional(GRU(128, return_sequences=True,dropout=0.1,recurrent_dropout=0.1))(title_layer)
#    title_layer     = Conv1D(64, kernel_size = 3, padding = "valid", kernel_initializer = "glorot_uniform")(title_layer)
#    avg_pool_title  = GlobalAveragePooling1D()(title_layer)
#    max_pool_title  = GlobalMaxPooling1D()(title_layer)
#    title_layer     = concatenate([avg_pool_title, max_pool_title])

    seq_description      = Input(shape=[500], name="seq_description")    #word2vec
    emb_seq_description  = Embedding(vocab_size2, EMBEDDING_DIM2, weights = [embedding_matrix2], trainable = True)(seq_description)
#    description_layer    = SpatialDropout1D(0.2)(emb_seq_description)  
#    description_layer    = Bidirectional(GRU(256, return_sequences=True,dropout=0.1,recurrent_dropout=0.1))(description_layer)
#    description_layer    = Conv1D(124, kernel_size = 3, padding = "valid", kernel_initializer = "glorot_uniform")(description_layer)
#    avg_pool_description = GlobalAveragePooling1D()(description_layer)
#    max_pool_description = GlobalMaxPooling1D()(description_layer)
#    description_layer    = concatenate([avg_pool_description, max_pool_description])
    title_layer       = GRU(128) (emb_seq_title)
    description_layer = GRU(128) (emb_seq_description)
#    text_layer = concatenate([title_layer, description_layer])
#    text_layer = Dense(64, activation='relu')(text_layer)

    ########## numeric layers ##########
    weekday               = Input(shape=[1], name="weekday")              #numerical
    price                 = Input(shape=[1], name="price")                #numerical
    item_seq_number       = Input(shape=[1], name='item_seq_number')      #numerical
    avg_ad_days           = Input(shape=[1], name="avg_ad_days")          #numerical
    avg_ad_times          = Input(shape=[1], name="avg_ad_times")         #numerical
    n_user_items          = Input(shape=[1], name="n_user_items")         #numerical

    ########## concatenate layers ##########
    main_layer = concatenate([
          categorical_layer
        , title_layer
        , description_layer
        , avg_ad_days
        , avg_ad_times
        , n_user_items
        , price
        , item_seq_number
        , weekday
    ])
    
    #main_layer = Dropout(0.1)(Dense(512,activation='relu') (main_layer))
    #main_layer = Dropout(0.1)(Dense(64,activation='relu') (main_layer))
    main_layer = Dense(256,activation='relu') (main_layer)
    main_layer = Dense(64,activation='relu') (main_layer)
    
    #output
    output = Dense(1,activation="sigmoid") (main_layer)
    
    #model
    model = Model([seq_title, seq_description, region, city, category_name, parent_category_name, param_1, param123, price, item_seq_number, image_code, weekday, avg_ad_days, avg_ad_times, n_user_items], output)
    model.compile(optimizer = 'adam',
                  loss= root_mean_squared_error,
                  metrics = [root_mean_squared_error])
    model.summary()
    return model

def rmse(y, y_pred):

    Rsum = np.sum((y - y_pred)**2)
    n = y.shape[0]
    RMSE = np.sqrt(Rsum/n)
    return RMSE 

def eval_model(model, X_test1):
    val_preds = model.predict(X_test1)
    y_pred = val_preds[:, 0]
    
    y_true = np.array(y_test1)
    
    yt = pd.DataFrame(y_true)
    yp = pd.DataFrame(y_pred)
    
    print(yt.isnull().any())
    print(yp.isnull().any())
    
    v_rmse = rmse(y_true, y_pred)
    print(" RMSE for VALIDATION SET: "+str(v_rmse))
    return v_rmse

exp_decay = lambda init, fin, steps: (init/fin)**(1/(steps-1)) - 1

def predictions(model):
    import time
    t1 = time.time()
    def load_test():
        for df in pd.read_csv(ds.test, chunksize= 100000):
            yield df

    item_ids = np.array([], dtype=np.int32)
    preds= np.array([], dtype=np.float32)

    i = 0 
    
    for df in load_test():
    
        i +=1
        print(df.dtypes)
        item_id = df['item_id']
        print(" Chunk number is "+str(i))
    
        test = preprocess_dataset(df)
    
        train_features = pd.read_csv(ds.aggregated_features)
        test = test.merge(train_features, on = ['user_id'], how = 'left')
        del train_features
        gc.collect()
    
        print(test.dtypes)
        
        test['avg_days_up_user'] = test['avg_days_up_user'].fillna(0).astype('uint32')
        test['avg_times_up_user'] = test['avg_times_up_user'].fillna(0).astype('uint32')
        test['n_user_items'] = test['n_user_items'].fillna(0).astype('uint32')
        test = keras_test_transform(test)
        del df
        gc.collect()
    
        print(test.dtypes)
    
        X_test = get_keras_data(test)
        del test 
        gc.collect()
    
        Batch_Size = 512*3
        preds1 = modelRNN.predict(X_test, batch_size = Batch_Size, verbose = 1)
        print(preds1.shape)
        del X_test
        gc.collect()
        print("RNN Prediction is done")

        preds1 = preds1.reshape(-1,1)
        #print(predsl.shape)
        preds1 = np.clip(preds1, 0, 1)
        print(preds1.shape)
        item_ids = np.append(item_ids, item_id)
        print(item_ids.shape)
        preds = np.append(preds, preds1)
        print(preds.shape)
        
    print("All chunks done")
    t2 = time.time()
    print("Total time for Parallel Batch Prediction is "+str(t2-t1))
    return preds 


train1 = np.array(train.values)
del train
gc.collect()

def get_data_frame(dataset):
    
    DF = pd.DataFrame()
    
    DF['avg_days_up_user']     = np.array(dataset[:,0])
    DF['avg_times_up_user']    = np.array(dataset[:,1])
    DF['category_name']        = np.array(dataset[:,2])
    DF['city']                 = np.array(dataset[:,3])
    DF['image_code']           = np.array(dataset[:,4])
    DF['item_seq_number']      = np.array(dataset[:,5])
    DF['n_user_items']         = np.array(dataset[:,6])
    DF['param123']             = np.array(dataset[:,7])
    DF['param_1']              = np.array(dataset[:,8])
    DF['parent_category_name'] = np.array(dataset[:,9])
    DF['price']                = np.array(dataset[:,10])
    DF['region']               = np.array(dataset[:,11])
    DF['weekday']              = np.array(dataset[:,12])
    DF['seq_title']            = np.array(dataset[:,13])
    DF['seq_description']      = np.array(dataset[:,14])
    print(DF.head())
    return DF 

from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
import time 
skf = KFold(n_splits = 5)
Kfold_preds_final = []
k = 0
RMSE = []

for train_idx, test_idx in skf.split(train1, y_train):
    
    print("Number of Folds.."+str(k+1))
    
    # Initialize a new Model #i#for Current FOLD 
    epochs = 5
    batch_size = 512
    steps = (int(train1.shape[0]/batch_size))*epochs
    lr_init, lr_fin = 0.009, 0.0045
    lr_decay = exp_decay(lr_init, lr_fin, steps)
    modelRNN = RNN_model()
    modelRNN.summary()
    K.set_value(modelRNN.optimizer.lr, lr_init)
    K.set_value(modelRNN.optimizer.decay, lr_decay)

    #K Fold Split 
    
    X_train1, X_test1 = train1[train_idx], train1[test_idx]
    print(X_train1.shape, X_test1.shape)
    print(X_train1[0])
    y_train1, y_test1 = y_train[train_idx], y_train[test_idx]
    print(y_train1.shape, y_test1.shape)
    gc.collect()
    
    print(type(X_train1))
    print(X_train1.shape)
    print(type(X_train1[:,14]))
    
    X_train_final = get_data_frame(X_train1)
    X_test_final = get_data_frame(X_test1)
    
    del X_train1, X_test1
    gc.collect()
    
    X_train_f = get_keras_data(X_train_final)
    X_test_f = get_keras_data(X_test_final)
    
    del X_train_final, X_test_final
    gc.collect()

    # Fit the NN Model 
    #for i in range(3):
    #    hist = modelRNN.fit(X_train_f, y_train1, batch_size=batch_size+(batch_size*(2*i)), epochs=epochs, validation_data=(X_test_f, y_test1), verbose=1)
    hist = modelRNN.fit(X_train_f, y_train1, batch_size=batch_size, epochs=epochs, validation_data=(X_test_f, y_test1), verbose=1)

    del X_train_f
    gc.collect()

    # Print RMSE for Validation set for Kth Fold 
    v_rmse = eval_model(modelRNN, X_test_f)
    RMSE.append(v_rmse)
    
    del X_test_f
    del y_train1, y_test1
    gc.collect()
    
    # Predict test set for Kth Fold 
    preds = predictions(modelRNN)
    del modelRNN 
    gc.collect()

    print("Predictions done for Fold "+str(k))
    print(preds.shape)
    Kfold_preds_final.append(preds)
    del preds
    gc.collect()
    print("Number of folds completed...."+str(len(Kfold_preds_final)))
    print(Kfold_preds_final[k][0:10])

print("All Folds completed"+str(k+1))   
print("RNN FOLD MODEL Done")

pred_final1 = np.average(Kfold_preds_final, axis =0) # Average of all K Folds
print(pred_final1.shape)

min_value = min(RMSE)
RMSE_idx = RMSE.index(min_value)
print(RMSE_idx)
pred_final2 = Kfold_preds_final[RMSE_idx]
print(pred_final2.shape)

#del Kfold_preds_final, train1
gc.collect()

pred_final1[0:5]

pred_final2[0:5]

test_cols = ['item_id']
test = pd.read_csv(ds.test, usecols = test_cols)

# using Average of KFOLD preds 

submission1 = pd.DataFrame( columns = ['item_id', 'deal_probability'])

submission1['item_id'] = test['item_id']
submission1['deal_probability'] = pred_final1

print("Check Submission NOW!!!!!!!!@")
submission1.to_csv("Avito_Shanth_RNN_AVERAGE.csv", index=False)

# Using KFOLD preds with Minimum value 
submission2 = pd.DataFrame( columns = ['item_id', 'deal_probability'])

submission2['item_id'] = test['item_id']
submission2['deal_probability'] = pred_final2

print("Check Submission NOW!!!!!!!!@")
submission2.to_csv("Avito_Shanth_RNN_MIN.csv", index=False)
