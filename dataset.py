import os

##### common path #####
DATA_HOME     = '/data/avito-demand-prediction'

##### csv files #####
periods_test      = os.path.join(DATA_HOME, 'periods_test.csv')
periods_train     = os.path.join(DATA_HOME, 'periods_train.csv')
test_active       = os.path.join(DATA_HOME, 'test_active.csv')
test              = os.path.join(DATA_HOME, 'test.csv')
train_active      = os.path.join(DATA_HOME, 'train_active.csv')
train             = os.path.join(DATA_HOME, 'train.csv')
sample_submission = os.path.join(DATA_HOME, 'sample_submission.csv')

##### jpg files #####
IMAGE_HOME    = os.path.join(DATA_HOME, 'data/competition_files')
train_jpg     = os.listdir(os.path.join(IMAGE_HOME, 'train_jpg'))
train_jpg_0   = os.listdir(os.path.join(IMAGE_HOME, 'train_jpg_0'))
train_jpg_1   = os.listdir(os.path.join(IMAGE_HOME, 'train_jpg_1'))
train_jpg_2   = os.listdir(os.path.join(IMAGE_HOME, 'train_jpg_2'))
train_jpg_3   = os.listdir(os.path.join(IMAGE_HOME, 'train_jpg_3'))
train_jpg_4   = os.listdir(os.path.join(IMAGE_HOME, 'train_jpg_4'))
test_jpg      = os.listdir(os.path.join(IMAGE_HOME, 'test_jpg'))

##### script files #####
SCRIPTS_HOME = '/home/ubuntu/apl/avito-demand-prediction'

##### Word2vec #####
aggregated_features   = os.path.join(DATA_HOME, 'nn/aggregated_features.csv')
avito_w2v             = os.path.join(DATA_HOME, 'nn/avito.w2v')
avito_w2v2            = os.path.join(DATA_HOME, 'nn/avito_v2.w2v')
avito_w2v3            = os.path.join(DATA_HOME, 'nn/avito_v3.w2v')
title300              = os.path.join(DATA_HOME, 'nn/title300.w2v')
title500              = os.path.join(DATA_HOME, 'nn/title500.w2v')
description500        = os.path.join(DATA_HOME, 'nn/description500.w2v')   
description1000       = os.path.join(DATA_HOME, 'nn/description1000.w2v')
title_description300  = os.path.join(DATA_HOME, 'nn/title_description300.w2v')
##### fasttext #####
fasttext2            = os.path.join(DATA_HOME, 'fasttext/avito_v2.ft.vec')
