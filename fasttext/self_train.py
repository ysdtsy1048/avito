import sys
sys.path.append('/home/ubuntu/apl/avito-demand-prediction/')
import dataset as ds
import os
import copy
import pandas as pd
from gensim.models import Word2Vec
#from random import shuffle
from keras.preprocessing.text import text_to_word_sequence
import numpy as np
import logging
#import time
from tqdm import tqdm
import gc

#logging.basicConfig(level=logging.INFO)
#use_cols   = ['param_1','param_2','param_3','title', 'description']
use_cols   = ['title', 'description']
chunk_size = 1000000
update     = False



def load_text(data, use_cols, chunk_size):
    print("loading...")
    df = pd.read_csv(data, usecols=use_cols, chunksize=chunk_size)
    return df

def preprocess(df, use_cols):
    #use_col = use_cols[0] 
    print("preprocessing...")
    df['title'].fillna(value='missing', inplace=True)
    df['description'].fillna(value='missing', inplace=True)
    #df['param_1'].fillna('', inplace=True)
    #df['param_2'].fillna('', inplace=True)
    #df['param_3'].fillna('', inplace=True)
    df["text"] = df["title"] + " " + df["description"]
    df["text"] = df["text"].astype(str)
    df["text"] = df["text"].str.lower() 
    #df.drop(use_cols, axis=1, inplace=True)
    df = df["text"].values
    df = [text_to_word_sequence(text) for text in tqdm(df)]
    return df

def build_model(data, use_cols, chunk_size, model, update):
    for df in load_text(data, use_cols, chunk_size):
        print("modeling...")
        df = preprocess(df, use_cols)
        model.build_vocab(df, update=update)
        model.train(df, total_examples=model.corpus_count, epochs=5)
        del df
        gc.collect()
        update = True
    return model, update

print("start!!")
model = Word2Vec(size=300, window=5)
model, update = build_model(ds.train, use_cols, chunk_size, model, update)
print("train Done!!!!")
model, update = build_model(ds.train_active, use_cols, chunk_size, model, update)
print("train active Done!!!!")
model, update = build_model(ds.test_active, use_cols, chunk_size, model, update)
print("test active Done!!!!")
model.save('title_description300.w2v')
