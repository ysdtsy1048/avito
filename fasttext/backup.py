import sys
sys.path.append('/home/ubuntu/apl/avito-demand-prediction/')
import dataset as ds
import os
import copy
import pandas as pd
#from gensim.models import Word2Vec
#from random import shuffle
#from keras.preprocessing.text import text_to_word_sequence
import numpy as np
import logging
#import time
from tqdm import tqdm


#logging.basicConfig(level=logging.INFO)
use_cols = ['param_1','param_2','param_3','title', 'description']

def load_text(data, use_cols):
    train_active = pd.read_csv(data,  usecols=use_cols)
    return df

def preprocess(df, use_cols):
    df['title'].fillna('', inplace=True)
    df['description'].fillna('', inplace=True)
    df['param_1'].fillna('', inplace=True)
    df['param_2'].fillna('', inplace=True)
    df['param_3'].fillna('', inplace=True)
    df["text"] = df["title"] + " " + df["description"] + " " + df["param_1"] + " " + df["param_2"] + " " + df["param_3"]
    df["text"] = df["text"].str.lower() 
    df.drop(use_cols, axis=1, inplace=True)
    return df

train_active, train = load_text(use_cols)
df = preprocess(pd.concat([train_active, train]), use_cols)
df.to_csv('text.txt', sep=' ', index=False, header=False)


#    train2['text'] = train2['param_1'].str.cat([train2.param_2,train2.param_3,train2.title,train2.description], sep=' ',na_rep='')
#    train2.drop(use_cols, axis = 1, inplace=True)
#    train2 = train2['text'].values
#    train2 = [text_to_word_sequence(text) for text in tqdm(train2)]
#    return train2

#model = Word2Vec(size=300, window=5,max_vocab_size=500000)

#for k in range(15):
#    update = False
#    if k != 0:
#        update = True
#    train = load_text(k*1000000+1)
#    model.build_vocab(train, update=update)
#    model.train(train, total_examples=model.corpus_count, epochs=3)

#model.save('avito_v2.w2v')
