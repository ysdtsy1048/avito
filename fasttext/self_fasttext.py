import sys
sys.path.append('/home/ubuntu/apl/avito-demand-prediction/')
import dataset as ds
import os
import copy
import pandas as pd
from gensim.models import Word2Vec
#from random import shuffle
#from keras.preprocessing.text import text_to_word_sequence
import numpy as np
import logging
#import time
from tqdm import tqdm
import gc

#logging.basicConfig(level=logging.INFO)
use_cols   = ['param_1','param_2','param_3','title', 'description']
chunk_size = 1000000
update     = False



def load_text(data, use_cols):
    print("loading...")
    df = pd.read_csv(data, usecols=use_cols)
    return df

def preprocess(df, use_cols):
    print("preprocessing...")
    df['title'].fillna('', inplace=True)
    df['description'].fillna('', inplace=True)
    df['param_1'].fillna('', inplace=True)
    df['param_2'].fillna('', inplace=True)
    df['param_3'].fillna('', inplace=True)
    df["text"] = df["title"] + " " + df["description"] + " " + df["param_1"] + " " + df["param_2"] + " " + df["param_3"]
    df["text"] = df["text"].str.lower() 
    df.drop(use_cols, axis=1, inplace=True)
#    df = df['text'].values
#    df = [text_to_word_sequence(text) for text in tqdm(df)]
    return df

def build_model(data, use_cols, chunk_size, model, update):
    for df in load_text(data, use_cols, chunk_size):
        print("modeling...")
        df = preprocess(df, use_cols)
        model.build_vocab(df, update=update)
        model.train(df, total_examples=model.corpus_count, epochs=3)
        del df
        gc.collect()
        update = True
    return model, update


train        = load_text(ds.train, use_cols)
train_active = load_text(ds.train_active, use_cols)
test_active  = load_text(ds.test_active, use_cols)
print("load done")
df = preprocess(pd.concat([train_active, train, test_active]), use_cols)
df.to_csv('/data/avito-demand-prediction/fasttext/text_v2.txt', sep=' ', index=False, header=False)
